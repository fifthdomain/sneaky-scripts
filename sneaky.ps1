# If you don't want a dialog for the SMB path set variable $SMBPATH to the path

$attackid = "attackartifact"

# Should Trigger:
# CAR-2013-01-003: SMB Events Monitoring
# CAR-2013-02-003: Processes Spawning cmd.exe
# CAR-2013-05-003: SMB Write Request
# CAR-2013-05-005: SMB Copy and Execution
# CAR-2013-09-003: SMB Session Setups
# CAR-2014-03-001: SMB Write Request - NamedPipes

$text = "echo Hello I am a spooky script!"
if (!$SMBPATH)
{
    [System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null
    $SMBPATH = [Microsoft.VisualBasic.Interaction]::InputBox("Enter a SMB path with write access:", "Computer", "\\Servername\share")
    echo $SMBPATH
}
$text | Set-Content -Force (Join-Path $SMBPATH ($attackid + ".bat"))
cmd.exe /c (Join-Path $SMBPATH ($attackid + ".bat"))

# Should Trigger:
# CAR-2013-03-001: Reg.exe called from Command Shell

cmd.exe /c reg.exe add /f HKLM\Software\$attackid

# Should Trigger:
# CAR-2013-04-002: Quick execution of a series of suspicious commands

ipconfig.exe
ping -n 1 8.8.8.8
tracert -h 1 8.8.8.8

# Should Trigger:
# CAR-2013-05-002: Suspicious Run Locations
Copy-Item -Force C:\Windows\System32\sethc.exe "C:\RECYCLER\sethc.exe"
"C:\RECYCLER\sethc.exe"

# Should Trigger:
# CAR-2013-05-009: Running executables with same hash and different names
Copy-Item -Force "C:\RECYCLER\sethc.exe" "C:\RECYCLER\"+$attackid+".exe"
"C:\RECYCLER\sethc.exe"
"C:\RECYCLER\"+$attackid+".exe"


# Should Trigger:
# CAR-2013-07-001: Suspicious Arguments
cmd.exe /c echo -R * -pw

# Should Trigger:
# CAR-2013-07-002: RDP Connection Detection
echo exit | telnet 127.0.0.1 3389

# Should Trigger:
# CAR-2013-07-005: Command Line Usage of Archiving Software
echo a $attackid

# Should Trigger:
# CAR-2013-08-001: Execution with schtasks
schtasks.exe

# Should Trigger:
# CAR-2014-04-003: Powershell Execution
cmd /c powershell.exe


# Should Trigger:
# CAR-2014-05-002: Services launching Cmd
# CAR-2013-09-005: Service Outlier Executables
# CAR-2014-11-002: Outlier Parents of Cmd
# CAR-2014-07-001: Service Search Path Interception
New-Service -Name $attackid -BinaryPathName "C:\WINDOWS\System32\svchost.exe -k cmd \c echo " + $attackid


# Should Trigger:
# CAR-2014-03-006: RunDLL32.exe monitoring
c:\windows\syswow64\rundll32.exe




# Should Trigger:
# CAR-2014-11-003: Debuggers for Accessibility Applications
echo sethc.exe
echo utilman.exe
echo osk.exe
echo narrator.exe
echo magnify.exe


# Should Trigger:
# CAR-2016-04-002: User Activity from Clearing Event Logs
Clear-EventLog -LogName "Windows PowerShell"

# Should Trigger:
# CAR-2016-03-001: Host Discovery Commands
hostname.exe
ipconfig.exe
net.exe
quser.exe
qwinsta.exe
systeminfo.exe
tasklist.exe
whoami.exe


# Should Trigger:
# CAR-2016-04-003: User Activity from Stopping Windows Defensive Services
Stop-Service -displayname "Windows Firewall"
Stop-Service -displayname "Windows Defender"


# Should Trigger:
# CAR-2016-04-004: Successful Local Account Login
cmd.exe /c net user $attackid $attackid /add  # Creates the user