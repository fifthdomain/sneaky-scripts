# README #

A script/set of scripts to run all attacks that are observed by the analytics in the [CAR Framework](https://car.mitre.org/)

### To Run ###

* Download the repository
* If you don't want a dialog for the SMB path set variable $SMBPATH to the path
* Run sneaky.ps1

### Implemented ###

* CAR-2013-01-003: SMB Events Monitoring
* CAR-2013-02-003: Processes Spawning cmd.exe
* CAR-2013-05-003: SMB Write Request
* CAR-2013-05-005: SMB Copy and Execution
* CAR-2013-09-003: SMB Session Setups
* CAR-2013-03-001: Reg.exe called from Command Shell
* CAR-2013-04-002: Quick execution of a series of suspicious commands
* CAR-2013-05-002: Suspicious Run Locations
* CAR-2013-05-009: Running executables with same hash and different names
* CAR-2013-07-001: Suspicious Arguments
* CAR-2013-07-002: RDP Connection Detection
* CAR-2013-07-005: Command Line Usage of Archiving Software
* CAR-2013-08-001: Execution with schtasks
* CAR-2014-04-003: Powershell Execution
* CAR-2014-05-002: Services launching Cmd
* CAR-2013-09-005: Service Outlier Executables
* CAR-2014-03-006: RunDLL32.exe monitoring
* CAR-2014-11-002: Outlier Parents of Cmd
* CAR-2014-07-001: Service Search Path Interception
* CAR-2016-04-002: User Activity from Clearing Event Logs
* CAR-2016-03-001: Host Discovery Commands
* CAR-2016-04-003: User Activity from Stopping Windows Defensive Services
* CAR-2014-11-003: Debuggers for Accessibility Applications	
* CAR-2013-08-001: Execution with schtasks
* CAR-2016-04-004: Successful Local Account Login
* CAR-2014-03-001: SMB Write Request - NamedPipes - Detected as subset of CAR-2013-01-003

### Todo ###
* CAR-2013-02-008: Simultaneous Logins on a Host
* CAR-2013-10-002: DLL Injection via Load Library
* CAR-2014-02-001: Service Binary Modifications
* CAR-2014-11-008: Command Launched from WinLogon
* Use a common term
* Dialog for clear event logs

### Unimplemented ###
* CAR-2013-05-004: Execution with AT - AT is not supported on most new systems
* CAR-2013-10-001: User Login Activity Monitoring - No Pseudocode
* CAR-2015-07-001: All Logins Since Last Boot - Not a detection analytic but instead a harm mitigation
* CAR-2013-02-012: User Logged in to Multiple Hosts - No Pseudocode


### Remote TODO ###
* CAR-2014-03-005: Remotely Launched Executables via Services
* CAR-2014-05-001: RPC Activity
* CAR-2014-11-004: Remote PowerShell Sessions
* CAR-2014-11-005: Remote Registry
* CAR-2014-11-006: Windows Remote Management (WinRM)
* CAR-2014-11-007: Remote Windows Management Instrumentation (WMI) over RPC
* CAR-2014-12-001: Remotely Launched Executables via WMI
* CAR-2015-04-001: Remotely Scheduled Tasks via AT
* CAR-2015-04-002: Remotely Scheduled Tasks via Schtasks
* CAR-2016-03-002: Create Remote Process via WMIC
* CAR-2016-04-005: Remote Desktop Logon
